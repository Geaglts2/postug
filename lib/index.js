"use strict";

require("core-js/modules/es.promise");

var _App = _interopRequireDefault(require("./App"));

var _dotenv = require("dotenv");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _dotenv.config)();

async function main() {
  try {
    const port = process.env.PORT;
    await _App.default.listen(port);
    console.log("servidor en el puerto ".concat(port));
  } catch (error) {
    console.log(error);
  }
}

main();