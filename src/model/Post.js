import Sequelize from "sequelize";
import { connection } from "../connection";

const Post = connection.define(
  "posts",
  {
    idpost: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    nombre: Sequelize.STRING(30),
    descripcion: Sequelize.STRING(60),
    idusuario: Sequelize.STRING(60)
  },
  { timestamps: false }
);

export default Post;
