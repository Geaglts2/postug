import Sequelize from "sequelize";
import { connection } from "../connection";

const User = connection.define(
  "usuarios",
  {
    idusuario: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    username: Sequelize.STRING(30),
    email: Sequelize.STRING(60),
    pass: Sequelize.STRING(150),
    llave: Sequelize.STRING(60),
    rol: Sequelize.STRING(25)
  },
  { timestamps: false }
);

export default User;
