export { default as authRoute } from "./Auth";
export { default as userRoute } from "./User";
export { default as postRoute } from "./Post";
