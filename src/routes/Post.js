import { Router } from "express";
const router = Router();

import {
  createPost,
  updatePost,
  getPosts,
  deletePost
} from "../controllers/Post";
import { isAuthenticate } from "../validations/auth";

router
  .route("/")
  .get(isAuthenticate, getPosts)
  .post(isAuthenticate, createPost);

router
  .route("/:idpost")
  .put(isAuthenticate, updatePost)
  .delete(isAuthenticate, deletePost);

export default router;
