import { config } from "dotenv";

config();

import App from "./App";

function main() {
  try {
    const port = process.env.PORT;
    App.listen(port, () => {
      console.log(`listening on ${port}`);
    });
  } catch (error) {
    console.log(error);
  }
}

main();
