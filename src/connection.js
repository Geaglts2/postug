import Sequelize from "sequelize";

/**
 * 
 "dc6u1t2cnrbds9",
  "ciysrayplovzek" ,
  "5c3196f4ea3d12ddc5e06e34388375577e164122bf4ac5e7c259b747e042cf87",
 * 
  dialectOptions: {
      ssl: {
        require: true
      }
    }
 * 
 * 
 * 
 */

export const connection = new Sequelize(
  "dc6u1t2cnrbds9",
  "ciysrayplovzek",
  "5c3196f4ea3d12ddc5e06e34388375577e164122bf4ac5e7c259b747e042cf87",
  {
    host: "ec2-107-21-214-26.compute-1.amazonaws.com",
    dialect: "postgres",
    port: 5432,
    ssl: true,
    dialectOptions: {
      ssl: {
        require: true
      }
    },
    logging: false
  }
);

const status = async () => {
  try {
    await connection.authenticate();
    console.log("conectado a la bd");
  } catch (error) {
    console.log("no se pudo conectar a la bd", error);
  }
};

status();
