import express, { json, urlencoded } from "express";
import "./connection";
const app = express();

app.use(json());
app.use(urlencoded({ extended: true }));

import { authRoute, userRoute, postRoute } from "./routes";

app.use("/api/auth", authRoute);
app.use("/api/user", userRoute);
app.use("/api/post", postRoute);

export default app;
