import Joi from "@hapi/joi";

const SchemaRegister = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(3)
    .required(),
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net"] }
    })
    .required(),
  pass: Joi.string().required()
});

const loginSchema = Joi.object({
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net"] }
    })
    .required(),
  pass: Joi.string().required()
});

export const registerValidate = async data => {
  try {
    await SchemaRegister.validateAsync(data);
    return true;
  } catch (error) {
    return error.details[0].message;
  }
};

export const loginValidate = async data => {
  try {
    await loginSchema.validateAsync(data);
    return true;
  } catch (error) {
    return error.details[0].message;
  }
};
