import Joi from "@hapi/joi";

const postSchema = Joi.object({
  nombre: Joi.string()
    .min(1)
    .required(),
  descripcion: Joi.string().required()
});

export const registerValidate = async data => {
  try {
    await postSchema.validateAsync(data);
    return true;
  } catch (error) {
    return error.details[0].message;
  }
};
