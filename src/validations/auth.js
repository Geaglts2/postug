import jwt from "jsonwebtoken";
import User from "../model/User";

export const isAuthenticate = (req, res, next) => {
  try {
    const token = req.headers.authorization;

    jwt.verify(token, process.env.SECRET_KEY, async (err, decoded) => {
      if (err) return res.sendStatus(403);

      const { idusuario } = decoded;

      const user = await User.findByPk(idusuario, {
        attributes: ["idusuario", "email", "pass", "llave"]
      });

      req.user = user;

      next();
    });
  } catch (error) {
    return res.sendStatus(403);
  }
};
