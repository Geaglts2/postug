import Crypto from "crypto";
import jwt from "jsonwebtoken";
import { registerValidate, loginValidate } from "../validations/validateInputs";
import User from "../model/User";

//Funcion que genera un token de sesion
const singToken = idusuario => {
  return jwt.sign({ idusuario }, process.env.SECRET_KEY, {
    expiresIn: 60 * 60 * 24 * 365
  });
};

//Funcion de registro
export const register = async (req, res) => {
  //recibe los datos del formulario
  let { email, pass, username } = req.body;

  //Pone el email en minusculas y le quita los espacios al inicio y al final
  email = email.toLowerCase().trim();

  //Valida si los campos son correctos
  const inputsState = await registerValidate({ email, pass, username });
  if (inputsState !== true) return res.json(inputsState);

  //Securidad para la pass
  Crypto.randomBytes(16, (err, buf) => {
    if (err) throw err;
    const llave = buf.toString("base64");
    Crypto.pbkdf2(pass, llave, 100000, 90, "sha1", async (err, derivedKey) => {
      if (err) throw err;
      const newPass = derivedKey.toString("base64");

      const modelUser = new User({
        username,
        email,
        pass: newPass,
        llave
      });

      //Busca si ese usuario ya existe
      const userExist = await User.findOne({ where: { email } });
      if (userExist) return res.json("usuario existente");

      const newUser = await User.create(modelUser, {
        fields: ["username", "email", "pass", "llave"]
      });

      newUser.save();

      res.json("registro exitoso");
    });
  });
};

//Funcion de logueo
export const login = async (req, res) => {
  //recibe los datos del formulario
  let { email, pass } = req.body;

  //Pone el email en minusculas y le quita los espacios al inicio y al final
  email = email.toLowerCase().trim();

  //Valida si los campos son correctos
  const inputsState = await loginValidate({ email, pass });
  if (inputsState !== true) return res.json(inputsState);

  //Verifica si el usuario existe
  const userExist = await User.findOne({
    where: { email },
    attributes: ["idusuario", "email", "pass", "llave"]
  });

  if (!userExist) return res.json("usuario y/o contraseña incorrecta");

  const llave = userExist.llave;

  //Verifica la pass
  Crypto.pbkdf2(pass, llave, 100000, 90, "sha1", async (err, derivedKey) => {
    if (err) throw err;

    const derivedPass = derivedKey.toString("base64");

    if (userExist.pass !== derivedPass)
      return res.json("usuario y/o contraseña  incorrecta");

    const token = singToken(userExist.idusuario);
    res.json({ token });
  });
};
