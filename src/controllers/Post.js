import Post from "../model/Post";
import { registerValidate } from "../validations/validatePost";

/**
 * crear un post, el post debe tener el id del usuario
 * mostrar los post solo de ese usuario
 * eliminar un post
 * actuaizar un post
 */

//create
export const createPost = async (req, res) => {
  const inputsState = await registerValidate(req.body);
  if (inputsState !== true) return res.json(inputsState);

  const { nombre, descripcion } = req.body;
  const { idusuario } = req.user;

  const modelPost = new Post({
    nombre,
    descripcion,
    idusuario
  });

  const newPost = await Post.create(modelPost, {
    fields: ["nombre", "descripcion", "idusuario"]
  });

  await newPost.save();

  res.json("guardado exitoso");
};

//get posts
export const getPosts = async (req, res) => {
  const { idusuario } = req.user;

  const Posts = await Post.findAll({
    where: { idusuario },
    attributes: ["idpost", "nombre", "descripcion"]
  });

  res.json(Posts);
};

//update function
export const updatePost = async (req, res) => {
  const { idpost } = req.params;
  const { nombre, descripcion } = req.body;

  const updatedPost = await Post.update(
    { nombre, descripcion },
    { where: { idpost } }
  );

  //Respuesta del servidor
  updatedPost[0] === 1
    ? res.json("se actualizo correctamente")
    : res.json("no se realizo ningun cambio");
};

//delete
export const deletePost = async (req, res) => {
  const { idpost } = req.params;

  const Posts = await Post.destroy({
    where: { idpost }
  });

  Posts === 1
    ? res.json("Se elimino con exito")
    : res.json("No se ha podido eliminar");
};
